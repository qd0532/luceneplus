package com.ld.zxw.config;


import java.util.Map;

import com.google.common.collect.Maps;

import redis.clients.jedis.Jedis;
/**
 * lucene 数据源
 * @author Administrator
 *
 */
public class LuceneDataSource {
	
	static LuceneDataSource kit = new LuceneDataSource();
	
	public Map<String, LucenePlusConfig> dataSource = null;
	
	public Jedis jedis = null;
	
	public static synchronized LuceneDataSource build() {
		if(kit.dataSource == null) {
			kit.dataSource = Maps.newHashMap();
		
		}
		return kit;
	}
	
	//默认不使用动态词典
	public boolean DynamicDictionary = false;
	
	private LuceneDataSource() {
	}
}
