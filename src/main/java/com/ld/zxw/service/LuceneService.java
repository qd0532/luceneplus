package com.ld.zxw.service;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;

import com.ld.zxw.page.Page;

/**
 * 接口
 * @author zxw
 *
 */
public interface LuceneService {

	/**
	 * 添加索引集合
	 * @param objs 原声 javaBean
	 * @throws IOException
	 */
	<T> void saveObj(List<T> objs) throws IOException;
	
	/**
	 * 添加索引对象
	 * @param obj 原声 javaBean
	 * @throws IOException
	 */
	<T> void saveObj(T obj) throws IOException;
	
	/**
	 * 添加索引集合
	 * @param documents
	 * @throws IOException
	 */
	<T> void saveDocument(List<Document> documents) throws IOException;
	/**
	 * 添加索引对象
	 * @param document
	 * @throws IOException
	 */
	<T> void saveDocument(Document document) throws IOException;

	/**
	 * 清空索引
	 * @throws IOException
	 */
	void delAll() throws IOException;

	/**
	 * 删除索引
	 * @param term
	 * @throws IOException
	 */
	void delKey(Term term) throws IOException;

	/**
	 * 删除索引
	 * @param query
	 * @throws IOException
	 */
	void delKey(Query query) throws IOException;

	/**
	 * 更新索引集合
	 * @param objs obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 */
	<T> void updateObj(List<T> objs, Term term) throws IOException;
	/**
	 * 更新索引对象
	 * @param obj obj 原声 javaBean
	 * @param term
	 * @throws IOException
	 */
	<T> void updateObj(T obj, Term term) throws IOException;
	
	/**
	 * 更新索引集合
	 * @param docs
	 * @param term
	 * @throws IOException
	 */
	void updateDocument(List<Document> docs, Term term) throws IOException;
	/**
	 * 更新索引对象
	 * @param doc
	 * @param term
	 * @throws IOException
	 */
	void updateDocument(Document doc, Term term) throws IOException;

	/**
	 * 查询索引集合
	 * @param query
	 * @param obj 原声 javaBean class
	 * @param num 条数 0 为所有
	 * @param sort 可以为 null
	 * @return
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 */
	<T> List<T> findList(Query query, Class<T> obj, int num, Sort sort) throws IOException, InvalidTokenOffsetsException;

	/**
	 * 查询对象集合分页
	 * @param query
	 * @param pageNumber 当前页  从 1开始
	 * @param pageSize 每页条数 0 为 10
	 * @param obj 原声 javaBean class
	 * @param sort 可以为 null
	 * @return
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 */
	<T> Page<T> findList(Query query, int pageNumber, int pageSize, Class<T> obj, Sort sort) throws IOException, InvalidTokenOffsetsException;

	
	
}
