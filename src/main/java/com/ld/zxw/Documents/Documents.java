package com.ld.zxw.Documents;

import java.util.Date;

import org.apache.lucene.document.BinaryPoint;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FloatPoint;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.BytesRef;
/**
 * 字段添加
 * @author Administrator
 *
 */
public class Documents {

	private Document document;

	public Documents() {
		document = new Document();
	}

	public void put(String key,Object val, String type, float boost){
		if(val != null) {
			if(type.equals("date")) {
				Date time = (Date) val;
				addField(key,Long.toString(time.getTime()), type, boost);
			}else {
				addField(key,val.toString(), type, boost);
			}
		}else {
			return;
		}
		
	}

	public Document getDocument(){
		return this.document;
	}



	/**
	 * 
	 * @param name key
	 * @param value val
	 * @param boost 权重
	 * @param type  字段类型
	 */
	public void addField(String name, String value,String type,float boost){
		switch (type) {
		case "int":
			int point_i = Integer.parseInt(value);
			document.add(new IntPoint(name, point_i));
			document.add(new StoredField(name, point_i));
			document.add(new NumericDocValuesField(name, point_i));
			break;
		case "integer":
			int point_it = Integer.parseInt(value);
			document.add(new IntPoint(name, point_it));
			document.add(new StoredField(name, point_it));
			document.add(new NumericDocValuesField(name, point_it));
			break;
		case "long":
			long point_l = Long.valueOf(value);
			document.add(new LongPoint(name, point_l));
			document.add(new StoredField(name, point_l));
			document.add(new NumericDocValuesField(name, point_l));
			break;
		case "date":
			long point_date = Long.valueOf(value);
			document.add(new LongPoint(name, point_date));
			document.add(new StoredField(name, point_date));
			document.add(new NumericDocValuesField(name, point_date));
			break;
		case "float":
			float point_f = Float.valueOf(value);
			document.add(new FloatPoint(name, point_f));
			document.add(new StoredField(name, point_f));
			break;
		case "binary":
			byte[] point_b = value.getBytes();
			document.add(new BinaryPoint(name, point_b));
			document.add(new StoredField(name, point_b));
			break;
		case "double":
			double point_d = Double.valueOf(value);
			document.add(new DoublePoint(name, point_d));
			document.add(new StoredField(name, point_d));
			break;
		case "string":
			document.add(new StringField(name, value, Field.Store.YES));
			document.add(new SortedDocValuesField(name, new BytesRef(value)));
			break;
		case "text":
			TextField textField = new TextField(name, value, Field.Store.YES);
			textField.setBoost(boost);
			document.add(textField);
			break;
		}
	}
}
