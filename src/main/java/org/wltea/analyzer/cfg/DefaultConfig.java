package org.wltea.analyzer.cfg;

/**
 * LucenePlusConfiguration 默认实现
 * 2012-5-8
 *
 */
public class DefaultConfig implements Configuration{

	/*
	 * 是否使用smart方式分词
	 */
	private boolean useSmart;
	
	/**
	 * 返回单例
	 * @return LucenePlusConfiguration单例
	 */
	public static  Configuration getInstance(){
		return new DefaultConfig();
	}
	
	/**
	 * 返回useSmart标志位
	 * useSmart =true ，分词器使用智能切分策略， =false则使用细粒度切分
	 * @return useSmart
	 */
	public boolean useSmart() {
		return useSmart;
	}

	/**
	 * 设置useSmart标志位
	 * useSmart =true ，分词器使用智能切分策略， =false则使用细粒度切分
	 * @param useSmart
	 */
	public void setUseSmart(boolean useSmart) {
		this.useSmart = useSmart;
	}	

}
